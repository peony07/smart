package com.cmig.smart.framework.demo.module.dao;

import com.cmig.smart.framework.demo.module.controller.test;

/**
 * @Author:zhangtao
 * @Description:
 * @Date Created in 10:45 2017/8/11.
 * @Modified by zhangtao on 10:45 2017/8/11.
 */
public class testExt extends test {

	private int b;

	public testExt(int a,int b) {
		super(a);
		this.b=b;
	}

	public static void main(String[] args) {
		testExt te=new testExt(1,2);
		te.printPublic();
		te.printProtected();
	}
}
