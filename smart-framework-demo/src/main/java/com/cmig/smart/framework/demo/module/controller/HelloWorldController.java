package com.cmig.smart.framework.demo.module.controller;

import com.alibaba.fastjson.JSONObject;
import com.cmig.smart.framework.annotation.Action;
import com.cmig.smart.framework.annotation.Controller;
import com.cmig.smart.framework.bean.Data;
import com.cmig.smart.framework.bean.Param;
import com.cmig.smart.framework.bean.View;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author:zhangtao
 * @Description:
 * @Date Created in 9:11 2017/8/7.
 * @Modified by zhangtao on 9:11 2017/8/7.
 */
@Controller(path = "/hello/")
public class HelloWorldController {


	@Action(path = "list",method = "get")
	public View getList(Param param){
		Map model=new HashMap<String,Object>();
		model.put("param1","param1_value");
		return new View("helloworld/list.jsp").addModel(model);
	}

	@Action(path = "info",method = "get")
	public Data getInfo(Param param){
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("key","1");
		jsonObject.put("value","2");
		Data data=new Data(jsonObject);
		return data;
	}
}
