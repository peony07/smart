package com.cmig.smart.framework.demo.module.controller;

/**
 * @Author:zhangtao
 * @Description:
 * @Date Created in 10:03 2017/8/11.
 * @Modified by zhangtao on 10:03 2017/8/11.
 */
public class test {

	public test(int a) {
		this.a = a;
	}

	private int a ;

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	void printDefault(){
		System.out.println("default");
	}

	public void printPublic(){
		System.out.println("public");
	}

	protected void printProtected(){
		System.out.println("protected");
	}
	private void printPrivate(){
		System.out.println("private");
	}

	public class bbb{
		void print(){
			System.out.println("bbb");
			printPrivate();
		}
	}

	private class ccc{
		void print(){
			System.out.println("ccc");
		}
	}

	protected class ddd{
		void print(){
			System.out.println("ddd");
		}
	}
}

 class aaa{

	 void print(){
		 System.out.println("aaa");
	 }

	 public static void main(String[] args) {
		 test t=new test(1);
		 t.printPublic();
		 t.printProtected();
		 t.printDefault();
	 }
}
