package com.cmig.smart.framework.helper;

import com.cmig.smart.framework.annotation.Controller;
import com.cmig.smart.framework.annotation.Entity;
import com.cmig.smart.framework.annotation.Service;
import com.cmig.smart.framework.utils.ClassUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class ClassHelper {

	private static final Logger logger = LoggerFactory.getLogger(ClassHelper.class);

	private static Set<Class<?>> classSet = new HashSet<Class<?>>();

	static {
		logger.debug("load class start ");
		String classPackageName = PropertiesHelper.getAppBasePackage();
		classSet = ClassUtil.getClasseSet(classPackageName);
		logger.debug("load class end ");
	}

	public static Set<Class<?>> getClassSet() {
		return classSet;
	}

	public static Set<Class<?>> getControllerClass() {
		Set<Class<?>> conteollerClassSet = new HashSet<Class<?>>();
		for (Class<?> cls : classSet) {
			if (cls.isAnnotationPresent(Controller.class)) {
				conteollerClassSet.add(cls);
			}
		}
		return conteollerClassSet;
	}


	public static Set<Class<?>> getServiceClass() {
		Set<Class<?>> serviceClassSet = new HashSet<Class<?>>();
		for (Class<?> cls : classSet) {
			if (cls.isAnnotationPresent(Service.class)) {
				serviceClassSet.add(cls);
			}
		}
		return serviceClassSet;
	}

	public static Set<Class<?>> getBeanClass() {
		Set<Class<?>> beanClassSet = new HashSet<Class<?>>();
		beanClassSet.addAll(getControllerClass());
		beanClassSet.addAll(getServiceClass());
		return beanClassSet;
	}

	public static Set<Class<?>> getEntityClass() {
		Set<Class<?>> entityClassSet = new HashSet<>();
		for (Class<?> cls : classSet) {
			if (cls.isAnnotationPresent(Entity.class)) {
				entityClassSet.add(cls);
			}
		}
		return entityClassSet;
	}

}
