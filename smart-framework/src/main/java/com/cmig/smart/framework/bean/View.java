package com.cmig.smart.framework.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class View {

    private String path;

    private Map<String ,Object> model;

    public View(String path) {
        this.path = path;
        this.model=new HashMap<>();
    }

    public View addModel(Map<String,Object> model){
        this.model=model;
	    return this;
    }

    public String getPath() {
        return path;
    }

    public Map<String, Object> getModel() {
        return model;
    }
}
