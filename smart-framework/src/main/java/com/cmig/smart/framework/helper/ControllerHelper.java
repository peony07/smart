package com.cmig.smart.framework.helper;

import com.cmig.smart.framework.annotation.Action;
import com.cmig.smart.framework.annotation.Controller;
import com.cmig.smart.framework.bean.Handler;
import com.cmig.smart.framework.bean.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class ControllerHelper {

    private  static final Logger logger= LoggerFactory.getLogger(ControllerHelper.class);

    private static Map<Request,Handler> controllerMap=new HashMap<>();

    static{
	    logger.debug("load controller request handler map start ");
        Set<Class<?>> controllerSet=ClassHelper.getControllerClass();
        for(Class cls:controllerSet){
	        Controller controller= (Controller) cls.getAnnotation(Controller.class);
            Method[] methods=cls.getMethods();
            for(Method method:methods){
                if(method.isAnnotationPresent(Action.class)){
                    Action action=method.getAnnotation(Action.class);
                    String requestMethodName=action.method();
                    String path=action.path();
                    Request request=new Request(controller.path()+path,requestMethodName);
                    Handler handler=new Handler(cls,method);
                    controllerMap.put(request,handler);
                }
            }
        }
	    logger.debug("load controller request handler map end ");
    }

    public static Handler getHandler(String path,String requestMethodName){
        Request request=new Request(path,requestMethodName);
        return controllerMap.get(request);
    }

    public static Handler getHandler(Request request){
        return controllerMap.get(request);
    }

}
