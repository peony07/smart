package com.cmig.smart.framework.helper;

import com.cmig.smart.framework.annotation.Column;
import com.cmig.smart.framework.annotation.Table;
import com.cmig.smart.framework.utils.MapUtil;
import com.cmig.smart.framework.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.Transient;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author:zhangtao
 * @Description:
 * @Date Created in 15:44 2017/8/9.
 * @Modified by zhangtao on 15:44 2017/8/9.
 */
public class EntityHelper {

	private  static final Logger logger= LoggerFactory.getLogger(EntityHelper.class);

	private  static final Map<Class<?>,String> classToTableNameMap=new HashMap<>();
	private  static final Map<Class<?>,Map<String,String>> classToColumnNameMap=new HashMap<>();

	static{
		logger.debug("load entity start ");
		Set<Class<?>> entityClassSet=ClassHelper.getEntityClass();
		if(null!=entityClassSet&&entityClassSet.size()>0){
			for(Class<?> cls:entityClassSet){
				String tableName;
				if(cls.isAnnotationPresent(Table.class)){
					Table table=cls.getAnnotation(Table.class);
					tableName=table.value();
				}else{
					tableName= StringUtil.camelhumpToUnderline(cls.getSimpleName());
				}
				logger.debug("entity {} -> table {}",cls.getSimpleName(),tableName);
				classToTableNameMap.put(cls,tableName);

				Field[] fields=cls.getFields();
				if(null!=fields&&fields.length>0){
					for(Field field:fields){
						//非数据库字段，跳过
						if(field.isAnnotationPresent(Transient.class)){
							continue;
						}
						Map<String,String> fieldToColumnMap=new HashMap<>();
						String columnName;
						String fieldName=field.getName();
						if(field.isAnnotationPresent(Column.class)){
							Column column=field.getAnnotation(Column.class);
							columnName=column.value();
						}else{
							columnName=StringUtil.camelhumpToUnderline(fieldName);
						}
						fieldToColumnMap.put(fieldName,columnName);
						logger.debug("fieldName {} -> columnName {}",fieldName,columnName);
						classToColumnNameMap.put(cls,fieldToColumnMap);
					}
				}
			}
		}
		logger.debug("load entity end ");
	}


	public String getTableName(Class cls){
		return classToTableNameMap.get(cls);
	}

	public Map<String,String> getFieleToColumnMap(Class cls){
		return classToColumnNameMap.get(cls);
	}

	public Map<String,String> getColumnToFieldName(Class cls){
		return MapUtil.invert(classToColumnNameMap.get(cls));
	}

	public String getColumnName(Class cls,String fieldName){
		Map<String,String> fieldToColumnMap=getFieleToColumnMap(cls);
		if(null!=fieldToColumnMap){
			return fieldToColumnMap.get(fieldName);
		}
		return null ;
	}

}
