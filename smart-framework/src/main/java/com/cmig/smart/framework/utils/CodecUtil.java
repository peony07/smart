package com.cmig.smart.framework.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class CodecUtil {

	private static final Logger logger = LoggerFactory.getLogger(CodecUtil.class);

	private static final String charset = "UTF-8";

	public static String encodeUrl(String source) throws UnsupportedEncodingException {
		String target = null;
		try {
			target = URLEncoder.encode(source, charset);
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
		return target;
	}

	public static String decodeUrl(String source) throws UnsupportedEncodingException {
		String target = null;
		try {
			target = URLDecoder.decode(source, charset);
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
		return target;
	}
}
