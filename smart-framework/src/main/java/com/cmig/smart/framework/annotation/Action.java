package com.cmig.smart.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author:zhangtao
 * @Description:
 * @Date Created in 17:35 2017/8/4.
 * @Modified by zhangtao on 17:35 2017/8/4.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {
	 String method() default "get";
	 String path() default "";
}
