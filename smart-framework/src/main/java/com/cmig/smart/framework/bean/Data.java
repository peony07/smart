package com.cmig.smart.framework.bean;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class Data {

    private Object model;

    public Data(Object model) {
        this.model = model;
    }

    public Object getModel() {
        return model;
    }
}
