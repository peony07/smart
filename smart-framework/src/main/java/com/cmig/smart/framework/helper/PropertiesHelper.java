package com.cmig.smart.framework.helper;

import com.cmig.smart.framework.conf.PropertiesLoader;
import com.cmig.smart.framework.constant.PropertiesContant;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class PropertiesHelper {

    private static PropertiesLoader propertiesLoader= new PropertiesLoader("smart-framework.properties");

    public static String getJdbcType(){
        return propertiesLoader.getProperty(PropertiesContant.SMART_FRAMEWORK_JDBC_TYPE);
    }

    public static String getJdbcDriver(){
        return propertiesLoader.getProperty(PropertiesContant.SMART_FRAMEWORK_JDBC_DRIVER);
    }

    public static String getJdbcUsername(){
        return propertiesLoader.getProperty(PropertiesContant.SMART_FRAMEWORK_JDBC_USERNAME);
    }

    public static String getJdbcPassword(){
        return propertiesLoader.getProperty(PropertiesContant.SMART_FRAMEWORK_JDBC_PASSWORD);
    }


    public static String getAppBasePackage(){
        return propertiesLoader.getProperty(PropertiesContant.SMART_FRAMEWORK_APP_BASE_PACKAGE);
    }
    public static String getAppJspPath(){
        return propertiesLoader.getProperty(PropertiesContant.SMART_FRAMEWORK_APP_HTML_PATH);
    }
    public static String getAppAssetPath(){
        return propertiesLoader.getProperty(PropertiesContant.SMART_FRAMEWORK_APP_ASSET_PATH);
    }

}
