package com.cmig.smart.framework.helper;

import com.cmig.smart.framework.utils.ReflectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class BeanHelper {

	private static final Logger logger = LoggerFactory.getLogger(BeanHelper.class);

	private static Map<Class, Object> beanMap = new HashMap<>();

	static {
		logger.debug("load bean start ");
		Set<Class<?>> beanSet = ClassHelper.getBeanClass();
		if (beanSet != null) {
			for (Class cls : beanSet) {
				Object object = ReflectionUtil.newInstance(cls);
				beanMap.put(cls, object);
			}
		}
		logger.debug("load bean end ");
	}

	public static Map<Class, Object> getBeanMap() {
		return beanMap;
	}

	public static <T> T getBean(Class<T> cls) {
		if (!beanMap.containsKey(cls)) {
			throw new RuntimeException("can not get bean by name " + cls);
		}
		return (T) beanMap.get(cls);
	}


}
