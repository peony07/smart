package com.cmig.smart.framework.helper;

import com.cmig.smart.framework.utils.ClassUtil;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class LoadHelper {

    public static void init(){
        Class<?>[] classArray=new Class[]{
            ClassHelper.class,
	        EntityHelper.class,
            BeanHelper.class,
            IocHelper.class,
            ControllerHelper.class};

	    try {
		    for(Class cls:classArray){
			    ClassUtil.loadClass(cls.getName(),false);
		    }
	    }catch (Exception e){
		    e.printStackTrace();
	    }
    }
}
