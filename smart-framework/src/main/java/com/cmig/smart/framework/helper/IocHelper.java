package com.cmig.smart.framework.helper;

import com.cmig.smart.framework.annotation.Inject;
import com.cmig.smart.framework.utils.ReflectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class IocHelper {

	private static final  Logger logger= LoggerFactory.getLogger(IocHelper.class);

    static{
	    logger.debug("load ioc start ");
        Map<Class,Object> beanMap=BeanHelper.getBeanMap();
        if(null!=beanMap){
            for(Map.Entry<Class,Object> beanEntry:beanMap.entrySet()){
                Class cls=beanEntry.getKey();
                Object instance=beanEntry.getValue();
                Field[] fields=cls.getFields();
                if(null!=fields&&fields.length>0) {
                    for (Field field : fields) {
                        if (field.isAnnotationPresent(Inject.class)) {
                            Object fieldInstance = beanMap.get(field.getType());
                            if (null != fieldInstance) {
                                ReflectionUtil.setFieldValue(instance, field, fieldInstance);
                            }
                        }
                    }
                }
            }
        }
	    logger.debug("load ioc end ");
    }
}
