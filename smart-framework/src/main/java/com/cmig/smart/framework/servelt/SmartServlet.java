package com.cmig.smart.framework.servelt;

import com.alibaba.fastjson.JSONObject;
import com.cmig.smart.framework.bean.Data;
import com.cmig.smart.framework.bean.Handler;
import com.cmig.smart.framework.bean.Param;
import com.cmig.smart.framework.bean.Request;
import com.cmig.smart.framework.bean.View;
import com.cmig.smart.framework.helper.BeanHelper;
import com.cmig.smart.framework.helper.ControllerHelper;
import com.cmig.smart.framework.helper.LoadHelper;
import com.cmig.smart.framework.helper.PropertiesHelper;
import com.cmig.smart.framework.utils.CodecUtil;
import com.cmig.smart.framework.utils.ReflectionUtil;
import com.cmig.smart.framework.utils.StreamUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author:zhangtao
 * @Description:
 * @Date Created in 17:42 2017/8/4.
 * @Modified by zhangtao on 17:42 2017/8/4.
 */
@WebServlet(urlPatterns = "/*",loadOnStartup = 0)
public class SmartServlet extends HttpServlet {

	private static final  Logger logger= LoggerFactory.getLogger(SmartServlet.class);

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
	    logger.debug("smart framework start ......");
        LoadHelper.init();

        ServletContext servletContext= servletConfig.getServletContext();
	    ServletRegistration jspServlet = servletContext.getServletRegistration("jsp");
	    if (StringUtils.isNotEmpty(PropertiesHelper.getAppJspPath())) {
		    jspServlet.addMapping(PropertiesHelper.getAppJspPath() + "*");
	    }

	    ServletRegistration assetServlet=servletContext.getServletRegistration("default");
	    assetServlet.addMapping("/index.html");
	    assetServlet.addMapping("/favicon.ico");
	    if (StringUtils.isNotEmpty(PropertiesHelper.getAppAssetPath())) {
		    assetServlet.addMapping(PropertiesHelper.getAppAssetPath() + "*");
	    }

	    logger.debug("smart framework end ......");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    req.setCharacterEncoding("UTF-8");

        String requestMethodName=req.getMethod().toLowerCase();
        String requestPath=req.getPathInfo();

	    // 将“/”请求重定向到首页
	    if (requestPath.equals("/")) {
		    resp.sendRedirect(req.getContextPath()+"/index.html");
		    return;
	    }

        Request request=new Request(requestPath,requestMethodName);
        Handler handler= ControllerHelper.getHandler(request);
        if(null!=handler){
            Class controllerClass=handler.getCls();
            Object controllerInstance= BeanHelper.getBean(controllerClass);

            Map<String,Object> paramMap=new HashMap<>();
            Enumeration<String> paramNames= req.getParameterNames();
            while(paramNames.hasMoreElements()){
                String paramName=paramNames.nextElement();
                String paramValue=req.getParameter(paramName);
                paramMap.put(paramName,paramValue);
            }

            String body= CodecUtil.decodeUrl(StreamUtil.getString(req.getInputStream()));
            if(StringUtils.isNoneEmpty(body)){
                String[] params=body.split("&");
                if(null!=params&&params.length>0){
                    for(String param:params){
                        if(param.split("=").length==2) {
                            String paramName = param.split("=")[0];
                            String paramValue = param.split("=")[1];
                            paramMap.put(paramName, paramValue);
                        }
                    }
                }
            }

            Param param=new Param(paramMap);
            Method method=handler.getMethod();
            Object result= ReflectionUtil.invokeMethod(controllerInstance,method,param);
            if(result instanceof View){
                View view=(View)result;
                String path=view.getPath();
                if(StringUtils.isNotEmpty(path)){
                    if(path.startsWith("/")){
                        resp.sendRedirect(req.getContextPath()+path);
                    }else{
	                    path=PropertiesHelper.getAppJspPath()+path;
	                    Map<String,Object> model=view.getModel();
	                    for(Map.Entry<String,Object>entry:model.entrySet()){
		                    req.setAttribute(entry.getKey(),entry.getValue());
	                    }
	                    logger.debug("path {}" ,path);
	                    req.getRequestDispatcher(path).forward(req,resp);
                    }
                }
            }else if(result instanceof Data){
                Data data=(Data)result;
                Object model=data.getModel();
                if(null!=model){
                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("UTF-8");
                    PrintWriter printWriter=resp.getWriter();
                    printWriter.print(JSONObject.toJSONString(model));
                    printWriter.flush();
                    printWriter.close();
                }
            }
        }
    }
}
