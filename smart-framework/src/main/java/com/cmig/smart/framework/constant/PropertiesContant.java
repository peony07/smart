package com.cmig.smart.framework.constant;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class PropertiesContant {

    public final static String SMART_FRAMEWORK_JDBC_TYPE ="jdbc.type";
    public final static String SMART_FRAMEWORK_JDBC_DRIVER ="jdbc.driver";
    public final static String SMART_FRAMEWORK_JDBC_USERNAME ="jdbc.username";
    public final static String SMART_FRAMEWORK_JDBC_PASSWORD ="jdbc.password";


    //基础包路径
    public final static String SMART_FRAMEWORK_APP_BASE_PACKAGE ="app.base.package";
    //jsp路径
    public final static String SMART_FRAMEWORK_APP_HTML_PATH ="app.jsp.path";
    //静态资源目录css js img
    public final static String SMART_FRAMEWORK_APP_ASSET_PATH ="app.asset.path";

}
